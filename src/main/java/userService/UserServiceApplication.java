package userService;

import authService.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import userService.services.HessianUserService;

@SpringBootApplication
public class UserServiceApplication {
    @Value("${authSrv.hessianApi}")
    private String hessianApi;

    @Bean(name = "authSrvInvoker")
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl(hessianApi);
        invoker.setServiceInterface(AuthService.class);
        return invoker;
    }

    @Autowired
    private HessianUserService hessianUserService;

    @Bean(name = "/usrSrv")
    RemoteExporter hessianService(HessianUserService hessianUserService) {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(hessianUserService);
        exporter.setServiceInterface(HessianUserService.class);
        return exporter;
    }

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}
