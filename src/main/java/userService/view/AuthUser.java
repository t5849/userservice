package userService.view;

@SuppressWarnings("unused")
public class AuthUser {
    private String userMail;

    private String password;

    public AuthUser() {}

    public AuthUser(String userMail, String password) {
        this.userMail = userMail;
        this.password = password;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "AuthUser{" +
                "userMail=" + userMail +
                ", password='" + password + '\'' +
                '}';
    }
}
