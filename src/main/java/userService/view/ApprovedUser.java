package userService.view;

import userService.model.postgresqlModels.User;

@SuppressWarnings("unused")
public class ApprovedUser {
    private User user;

    private String accessToken;

    public ApprovedUser() {

    }

    public ApprovedUser(User user, String accessToken) {
        this.user = user;
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public String toString() {
        return "ApprovedUser{" +
                "user=" + user +
                ", accessToken='" + accessToken + '\'' +
                '}';
    }
}
