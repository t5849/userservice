package userService.view;

@SuppressWarnings("unused")
public class AlterPasswordUser {
    private Long userId;

    private String token;
    private String password;

    public AlterPasswordUser() {}

    public AlterPasswordUser(Long userId, String token, String password) {
        this.userId = userId;
        this.token = token;
        this.password = password;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "AlterPasswordUser{" +
                "userId=" + userId +
                ", token='" + token + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
