package userService.view;

import userService.model.postgresqlModels.User;

@SuppressWarnings("unused")
public class NewUser {
    private User user;

    private String password;

    public NewUser() {}

    public NewUser(User user, String password) {
        this.user = user;
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "NewUser{" +
                "user=" + user +
                ", password='" + password + '\'' +
                '}';
    }
}
