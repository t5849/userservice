package userService.controllers;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import userService.model.postgresqlModels.User;
import userService.services.UserService;
import userService.view.AlterPasswordUser;
import userService.view.ApprovedUser;
import userService.view.AuthUser;
import userService.view.NewUser;

import java.util.Arrays;

@RestController
@Validated
@SuppressWarnings("unused")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user/register")
    public User registerUser(@RequestBody NewUser user) {
        return userService.registerUser(user);
    }

    @PostMapping("/user/login")
    public ApprovedUser loginUser(@RequestBody AuthUser user) {
        return userService.loginUser(user);
    }

    @PutMapping("/user/update")
    public User updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @PutMapping("/user/changePassword")
    public User changePassword(@RequestBody AlterPasswordUser user) {
        return userService.changePassword(user);
    }

    @GetMapping("/user/getByUserId")
    public User getUserByUserId(@RequestParam Long userId) {
        return userService.getUserByUserId(userId);
    }

    @GetMapping("/user/getByUserMail")
    public User getUserByUserMain(@RequestParam String userMail) {
        return userService.getUserByUserMail(userMail);
    }

    @GetMapping("/user/getUsersByUserIds")
    public User[] getUserNamesByUserIds(@RequestParam Long[] userIds) {
        return Arrays.
                stream(userIds).
                map(userService::getUserByUserId).
                toArray(User[]::new);
    }

    @PostMapping("/user/all")
    public User[] getUsers()
    {
        return userService.getUsers();
    }
}
