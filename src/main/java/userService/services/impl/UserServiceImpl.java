package userService.services.impl;

import authService.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import userService.model.postgresqlModels.User;
import userService.repositories.postgersqlRepos.UserRepository;
import userService.services.HessianUserService;
import userService.services.UserService;
import userService.view.AlterPasswordUser;
import userService.view.ApprovedUser;
import userService.view.AuthUser;
import userService.view.NewUser;

import java.util.Objects;
import java.util.stream.StreamSupport;

@Service
@DependsOn("authSrvInvoker")
@SuppressWarnings("unused")
public class UserServiceImpl implements HessianUserService, UserService {
    private final UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${authSrv.registerUrl}")
    private String authSrvRegisterUrl;

    @Value("${authSrv.loginUrl}")
    private String authSrvLoginUrl;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User registerUser(NewUser user) {
        User dbUser = null;
        try {
            dbUser = userRepository.save(user.getUser());
            makeCredentials(dbUser.getUserId(), user.getPassword());
            return dbUser;
        } catch (Exception e) {
            if (dbUser != null)
            {
                userRepository.delete(dbUser);
            }
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ApprovedUser loginUser(AuthUser user) {
        try {
            User dbUser = userRepository.findByUserMail(user.getUserMail()).orElseThrow(IllegalArgumentException::new);
            String accessToken = checkCredentials(dbUser.getUserId(), user.getPassword());
            return new ApprovedUser(dbUser, accessToken);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void makeCredentials(Long userId, String password) throws Exception {
        authService.register(userId.toString(), password);
    }

    private String checkCredentials(Long userId, String password) throws Exception {
        return authService.login(userId.toString(), password);
    }

    @Override
    public User updateUser(User user) {
        try {
            return userRepository.findById(user.getUserId()).map(dbUser -> {
                dbUser.setUserName(user.getUserName());
                dbUser.setUserMail(user.getUserMail());
                dbUser.setQiwiName(user.getQiwiName());
                userRepository.save(dbUser);
                return dbUser;
            }).orElse(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User changePassword(AlterPasswordUser user)  {
        try {
            this.authService.resetPassword(user.getToken(), user.getPassword());
            return userRepository.findByUserId(user.getUserId()).orElseThrow(IllegalArgumentException::new);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User getUserByUserId(Long userId) {
        try {
            return userRepository.findByUserId(userId).orElse(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User getUserByUserMail(String userMail) {
        try {
            return userRepository.findByUserMail(userMail).orElse(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User[] getUsers() {
        return StreamSupport.
                stream(userRepository.findAll().spliterator(), false).
                toArray(User[]::new);
    }

    @Override
    public boolean exists(Long userId) {
        return Objects.nonNull(this.getUserByUserId(userId));
    }
}
