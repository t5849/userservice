package userService.services;

public interface HessianUserService {
    boolean exists(Long userId);
}
