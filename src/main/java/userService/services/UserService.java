package userService.services;

import userService.model.postgresqlModels.User;
import userService.view.AlterPasswordUser;
import userService.view.ApprovedUser;
import userService.view.AuthUser;
import userService.view.NewUser;

public interface UserService {
    User registerUser(NewUser user);
    ApprovedUser loginUser(AuthUser user);
    User updateUser(User course);
    User changePassword(AlterPasswordUser user);
    User getUserByUserId(Long userId);
    User getUserByUserMail(String userMail);
    User[] getUsers();
}
