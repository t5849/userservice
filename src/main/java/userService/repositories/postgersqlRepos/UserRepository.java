package userService.repositories.postgersqlRepos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import userService.model.postgresqlModels.User;

import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByUserId(Long userId);
    Optional<User> findByUserMail(String userMail);
}
