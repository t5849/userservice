package userService.model.postgresqlModels;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Table(name = "users")
@SuppressWarnings("unused")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", nullable = false, unique = true, updatable = false)
    private Long userId;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "user_mail", nullable = false, unique = true)
    @Email(regexp = ".+@.+\\..+")
    private String userMail;

    @Column(columnDefinition = "varchar(255) default ''")
    private String qiwiName;

    public User() {}

    public User(String userLogin, String userName, String userMail, String qiwiName) {
        this.userName = userName;
        this.userMail = userMail;
        this.qiwiName = qiwiName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userMail='" + userMail + '\'' +
                '}';
    }

    public String getQiwiName() {
        return qiwiName;
    }

    public void setQiwiName(String qiwiName) {
        this.qiwiName = qiwiName;
    }

}
